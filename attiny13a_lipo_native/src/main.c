
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#include <util/delay.h>

#define BAT_MIN 660
//#define BAT_MIN 500

#define LED PORTB2
#define MOSFET PORTB3

volatile int g_toggle = 0;

#define MODE_OK 0
#define MODE_EMPTY 1

uint8_t g_mode = 0;



ISR(WDT_vect) {
  if(g_toggle == 0) {
    g_toggle = 1;
  }
}

void init_gpio()
{
  // GPIO Output
  DDRB |= _BV(LED);
  DDRB |= _BV(MOSFET);

  // LED off
  PORTB &= ~_BV(LED);
  // Mosfet on (faster startup)
  PORTB |= _BV(MOSFET);
}

void init_wdt()
{
  MCUSR &= ~(1<<WDRF);             /* WDT reset flag loeschen */
  WDTCR |= (1<<WDCE) | (1<<WDE);  /* WDCE setzen, Zugriff auf Presclaler etc. */
  WDTCR = 1<<WDP0 | 1<<WDP3;      /* Prescaler auf 8.0 s */
  WDTCR |= 1<<WDTIE;               /* WDT Interrupt freigeben */
}

void enableAnalog()
{
  power_adc_enable();
  //ACSR = _BV(ACD); // Disable comparator
  DIDR0 = 0x3F; // Disable digital input buffer

  // Internal ref && ADC2
  ADMUX |= _BV(REFS0);
  ADMUX = (ADMUX & 0xfc) | _BV(MUX1);
  ADCSRA = _BV(ADPS1) | _BV(ADPS0);

  ADCSRA |= _BV(ADEN);
  ADCSRA |= _BV(ADSC);
  while( ADCSRA & _BV(ADSC) ) {}; // Wait for end
  ADC;
}

void disableAnalog()
{
  power_adc_disable();
  ADCSRA &= ~_BV(ADEN);
}

void enter_sleep()
{
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  cli();
  sleep_enable();
  sleep_bod_disable();
  disableAnalog();
  sei();
  sleep_cpu();
  sleep_disable();

  enableAnalog();
}

uint16_t readAnalog()
{
  unsigned long sum = 0;
  for(int a=0; a<8; ++a) {
    ADCSRA |= _BV(ADSC);
    while( ADCSRA & _BV(ADSC) ) {}; // Wait for end
    sum += ADC;
  }
  return sum / 8;
}

void blinkLed()
{
  PORTB |= _BV(LED);
  _delay_ms(100);
  PORTB &= ~_BV(LED);
}

void disableMosfet()
{
  PORTB &= ~_BV(MOSFET);
}

void loop()
{
  enter_sleep();

  if(g_toggle) {
    if(g_mode == MODE_OK) {
      int analog = readAnalog();
      if(analog < BAT_MIN) {
        disableMosfet();
        g_mode = MODE_EMPTY;
        blinkLed();
      }
    } else {
      blinkLed();
    }
  }
}



int main(void)
{
  cli();

  power_all_disable();

  init_gpio();
  init_wdt();
  enableAnalog();

  g_mode = MODE_OK;

  sei();

  for(;;) {
    loop();
  }
}
